# News Crawler

A news crawler which fetches news about the keyword you provide.

## What it does

These codes fetch and crawl news from internet based on search terms
given. They receive news from "Google News", "The Guardian" and
"NewYork Times" websites.  
Fetched news are stored in CSV files in current working directory.

~~Also, there is a "fa" version which seeks for persian news.~~ This file
has been removed due to duplicate code with non-fa version and
non-functioning code. (It doesn't crawl persian news only. It fetches
anything)

## Project history

About a year ago I wrote these codes for a private project but I forgot to
FLOSS them.

Due to probable changes to websites crawled, they may not work today but,
I believe the code is clean enough to be refactored and debuged to respect
any chages happend to websites used.

## Licensing

See: [LICENSE](LICENSE)

tl;dr: MIT (Expat) License  
Long story: At first, I wanted to license these under AGPL but these
are very common and simple programs which could be written by anybody!
(even me myself used a lot of FLOSS codes to write these) So I decided
to publish this repo under a more permassive and simpler license and hence
I choosed "MIT License".
